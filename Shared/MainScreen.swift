//
//  ContentView.swift
//  Shared
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

private enum SelectedScreen: CaseIterable {
    case dashboard
    case lastMonth
    case note
}

struct MainScreen: View {
    private var dashboardViewModel = LastMonthViewModel()
    @State private var selection: SelectedScreen = .dashboard

    var body: some View {
        TabView(selection: $selection) {
            ForEach(SelectedScreen.allCases, id: \.self) { type in
                switch type {
                case .dashboard:
                    DashboardScreen {
                        selection = .lastMonth
                        dashboardViewModel.selection = 0
                    }
                    .tabItem {
                        Image(systemName: "bag")
                        Text("Dashboard")
                    }
                case .note:
                    NoteScreen()
                        .tabItem {
                            Image(systemName: "note.text")
                            Text("Note")
                        }
                case .lastMonth:
                    LastMonthScreen()
                        .tabItem {
                            Image(systemName: "calendar")
                            Text("Last Month")
                        }
                }
            }
        }
        .environmentObject(dashboardViewModel)
    }
}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}
