//
//  NoteScreen.swift
//  FirstHomework
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

final class NoteViewModel: ObservableObject {
    @Published var text: NSAttributedString?
    @Published var isEditorPresented = false
}

struct NoteScreen: View {
    @StateObject var viewModel = NoteViewModel()
    @State var text = ""
    var body: some View {
        VStack {
            ScrollView {
                AttributedStringLabel(text: $viewModel.text)
                    .padding()
                Spacer()
            }
            Button("Edit") {
                viewModel.isEditorPresented = true
            }
            .frame(height: 40)
            .sheet(isPresented: $viewModel.isEditorPresented) {
                RichTextEditor(text: $viewModel.text)
                    .padding()
            }
        }
    }
}

struct NoteScreen_Previews: PreviewProvider {
    static var previews: some View {
        NoteScreen()
    }
}
