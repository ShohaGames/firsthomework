//
//  LastMonthViewModel.swift
//  FirstHomework
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import Foundation

final class LastMonthViewModel: ObservableObject {
    struct Day {
        let short: String
        let long: String
    }

    @Published var selection: Int?
    let days: [Day] = (0..<30).compactMap { index in
        guard
            let date = Calendar.current.date(byAdding: .day, value: -index, to: Date())
        else {
            return nil
        }

        return Day(
            short: shortDateFormatter.string(from: date),
            long: longDateFormatter.string(from: date)
        )
    }

    private static let shortDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter
    }()
    private static let longDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        return dateFormatter
    }()
}
