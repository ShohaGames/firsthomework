//
//  LastMonthScreen.swift
//  FirstHomework
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

struct LastMonthScreen: View {
    @EnvironmentObject private var viewModel: LastMonthViewModel

    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: .leading, spacing: 4) {
                    ForEach(Array(viewModel.days.enumerated()), id: \.offset) { index, day in
                        Button(day.short) {
                            viewModel.selection = index
                        }
                        .frame(maxWidth: .infinity, minHeight: 40)
                        .background(viewModel.selection == index ? Color(white: 0.9) : Color.clear)
                        NavigationLink(
                            tag: index,
                            selection: $viewModel.selection,
                            destination: { Text("\(day.long)") }
                        ) { EmptyView() }
                    }
                }
                .frame(maxWidth: .infinity)
            }
            .navigationTitle("Last Month")
        }
    }
}

struct LastMonthScreen_Previews: PreviewProvider {
    static var previews: some View {
        LastMonthScreen()
    }
}
