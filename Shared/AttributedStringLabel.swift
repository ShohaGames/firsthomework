//
//  UIKLabel.swift
//  FirstHomework
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

struct AttributedStringLabel: UIViewRepresentable {
    @Binding var text: NSAttributedString?

    func makeUIView(context: Context) -> UILabel {
        let uiView = UILabel()
        uiView.setContentHuggingPriority(.required, for: .vertical)
        uiView.numberOfLines = 0
        return uiView
    }

    func updateUIView(_ uiView: UILabel, context: Context) {
        uiView.attributedText = text
    }
}
