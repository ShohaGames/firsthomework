//
//  DashboardScreen.swift
//  FirstHomework
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

struct DashboardScreen: View {

    private let didTapHandler: () -> Void

    init(didTapHandler: @escaping () -> Void) {
        self.didTapHandler = didTapHandler
    }

    var body: some View {
        Button {
            didTapHandler()
        } label: {
            Text("Go To Today!")
        }
    }
}

struct DashboardScreen_Previews: PreviewProvider {
    static var previews: some View {
        DashboardScreen(didTapHandler: {})
    }
}
