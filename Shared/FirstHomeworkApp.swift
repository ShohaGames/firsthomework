//
//  FirstHomeworkApp.swift
//  Shared
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

@main
struct FirstHomeworkApp: App {
    var body: some Scene {
        WindowGroup {
            MainScreen()
        }
    }
}
