//
//  RichTextEditor.swift
//  FirstHomework
//
//  Created by Constantine Navrosjuk on 25.02.22.
//

import SwiftUI

final class RichTextEditorCoordinator: NSObject, UITextViewDelegate {
    private var view: RichTextEditor

    init(_ view: RichTextEditor) {
        self.view = view
    }

    func textViewDidChange(_ textView: UITextView) {
        view.text = textView.attributedText
    }
}

struct RichTextEditor: UIViewRepresentable {
    @Binding var text: NSAttributedString?

    func makeCoordinator() -> RichTextEditorCoordinator {
        RichTextEditorCoordinator(self)
    }

    func makeUIView(context: Context) -> UITextView {
        let uiView = UITextView()
        uiView.allowsEditingTextAttributes = true
        uiView.delegate = context.coordinator
        return uiView
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.attributedText = text
        uiView.delegate = context.coordinator
    }
}
